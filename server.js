var express = require("express");
var app = require("express")();
var bodyParser = require("body-parser");
var mysql = require("mysql");
var cookieParser = require("cookie-parser");
var crypto = require("crypto");
var session = require("express-session");
var http = require("http").Server(app);
var io = require("socket.io")(http);

//var apps = express();

app.use(express.static(__dirname + "/static"));
//app.set('views', __dirname + '/static/views');

app.use(session ({
	secret: "secret",
	resave: true,
	saveUninitialized: true
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.engine('html', require('ejs').renderFile);

var connection = mysql.createConnection ({
	host: "localhost",
	user: "root",
	password: "",
	database: "ivor"
});

setInterval(function () {
    connection.query("SELECT 1");
}, 5000);

io.on("connection", function(socket){
  socket.on("chat message", function(msg, room){
  	console.log("room", room);
  	io.emit("chat message" + room, msg);
    console.log("message: " + msg);
  });
});

http.listen(3000, function () {
	console.log("Listening on port 3000");
});

app.get("/", function (request, response) {
	response.redirect("/login");
});

app.get("/logout", function (request, response) {
	var username = request.session.username;
	request.session.destroy();
	response.redirect("/");
});

app.post("/authHome", function (request, response) {
	var data = request.body.data;

	if (data == "home"){
		var sess = request.session;
		var responseData = {username: sess.username, sessid: sess.sessid};
		response.send(responseData);
	}
});

app.get("/home", function (request, response) {
	if (request.session.sessid){
		response.sendFile(__dirname + "/static/views/home.html");
	}else{
		response.redirect("/login");
	}
});

app.post("/returnInfo", function (request, response) {
	if (request.body.code == 123) response.send({data: request.session});
	else response.status(401).json({error: "Authorization at returnInfo"});
});

////////////* Login routes START *////////////

app.get("/login", function (request, response) {
	if (request.session.sessid){
		response.redirect("/home");
	}else{
		response.sendFile(__dirname + "/static/views/login.html");
	}
});

app.post("/login", function (request, response) {
	var username = request.body.username;
	var password = request.body.password;
	var sess = request.session;

	if (username != undefined && password != undefined) {

		/* if (userExists(username)){
			var query = connection.query("SELECT * FROM users WHERE username='" + username + "';", function(error, result) {
				//console.log(result);
			});
		} else {
			console.log("User doesn't exists!");
		} */
		userExists(username, function (error, data) {
			var returns = 0;
			if (error) {
				console.log("USER EXISTS ERROR : ",err);   
			} else {
				db_username = "prazno";
				db_password = "prazno";
				db_sessid = "prazno";
				if (data == username) {
					
					var query = connection.query("SELECT * FROM users WHERE username='" + username + "';", function (error, result) {
						var str = JSON.stringify(result);
						result = JSON.parse(str);
						
						var db_username = result[0].username;
						var db_password = result[0].password;
						var db_sessid = result[0].sessid;

						if (db_username == username && db_password == password){
							console.log("User: '" + username + "' successfully logged in!");
							request.session.sessid = db_sessid;
							request.session.username = db_username;
							response.redirect("/home");
						}else{
							console.log("User: '" + username + "' entered wrong password!");
							response.redirect("/login");
						}
						//response.render("login.html");
					});

				} else {
					console.log("Entered user: '" + username + "' does not exist!");
					response.redirect("/login");
				}
			}
		});

	} else {
		console.log("Username: '" + username + "', password: '" + password + "', one of them is undefined");
		response.redirect("/login");
	}
});

////////////* Login routes END *//////////////


////////////* Register routes START *////////////

app.get("/register", function (request, response) {
	if (request.session.sessid){
		response.redirect("/home");
	}else{
		response.sendFile(__dirname + "/static/views/register.html");
	}
});

app.post("/register", function (request, response) {
	var username = request.body.username;
	var password = request.body.password;

	if (username != undefined && password != undefined) {

		// FOR some reason, this is unnecessary

		var sessid = randomValueHex(32);
		var post = {id: null, username: username, password: password, sessid: sessid};
		var query = connection.query("INSERT INTO users SET ?", post, function (error, result) {
			if (error) {
				if (error.code == "ER_DUP_ENTRY") {
					console.log(error.code, "for user:", username);

					response.redirect("/register");
				}else{
					console.log(error);
					response.redirect("/register");
				}
			} else {
				console.log("User successfully added to database!");
				response.redirect("/home");
				response.end();
			}
		});
	} else {
		//response.sendFile(__dirname + "/static/views/error.html?error=925");
		response.redirect("/register");
	}
});

////////////* Register routes END *//////////////

function randomValueHex (len) {
	return crypto.randomBytes(Math.ceil(len/2))
	.toString('hex') // convert to hexadecimal format
	.slice(0,len);   // return required number of characters
}

function userExists (username, callback) {
	var query = connection.query("SELECT username FROM users WHERE username='" + username + "';", function (error, result) {
		var str = JSON.stringify(result);
		result = JSON.parse(str);
		if (result[0]) {
			if (result[0].username == username) {
				callback(null,result[0].username);
			} else {
				callback(null,null);
			}
		} else {
			callback(null,null);
		}
	});
}
